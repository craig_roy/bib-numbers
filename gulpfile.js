var gulp = require('gulp');
var watch = require('gulp-watch');
var sourcemaps = require('gulp-sourcemaps');
var notify   = require('gulp-notify');
var coffee = require('gulp-coffee');
var coffeelint = require('gulp-coffeelint');
var sass = require('gulp-ruby-sass');
var phpcs = require('gulp-phpcs');
var codecept = require('gulp-codeception');

gulp.task('coffee-lint', function () {
    gulp.src('./web/js/coffee/*.coffee')
        .pipe(coffeelint())
        .pipe(coffeelint.reporter())
});

gulp.task('coffee', function() {
    gulp.src('./web/js/coffee/*.coffee')
        .pipe(coffee({bare: true}))
        .on('error', notify.onError({
            title: "Failed to compile Coffee",
            message: "Something went wrong when trying to compile your coffee"
        }))
        .pipe(gulp.dest('./web/js/'))
        .pipe(notify({
            title: "Coffee Compiled Successfully",
            message: "Your coffee has been successfully compiled YAY!!"
        }));

});

gulp.task('sass', function () {
    return gulp.src('./web/css/sass/*.sass')
        .pipe(sass({sourcemap: false, sourcemapPath: '../scss'}))
        .on('error', notify.onError({
            title: "Failed to compile sass",
            message: "Something went wrong when trying to compile your sass"
        }))
        .pipe(gulp.dest('./web/css/'))
        .pipe(notify({
            title: "sass Compiled Successfully",
            message: "Your sass has been successfully compiled YAY!!"
        }));
});

gulp.task('codecept', function() {
    var options = {notify: true};
    gulp.src('./tests/*.php')
        .pipe(codecept('', options))
        .on('error', notify.onError({
            title: "Testing Failed",
            message: "Error(s) occurred during test..."
        }))
        .pipe(notify({
            title: "Testing Passed",
            message: "All tests have passed..."
        }));
});

gulp.task('phpcs', function () {
    return gulp.src(['./model/*/*.php', './views/*/*.php', './controllers/*/*.php', './commands/*/*.php'])
        // Validate files using PHP Code Sniffer
        .pipe(phpcs({
            bin: './vendor/squizlabs/php_codeSniffer/scripts/phpcs',
            standard: 'PSR2',
            warningSeverity: 0
        }))
        // Log all problems that was found
        .pipe(phpcs.reporter('log'));
});

gulp.task('watch', function(){
    gulp.watch('./web/js/coffee/*.coffee', ['coffee-lint', 'coffee']);
    gulp.watch('./web/css/sass/*.sass', ['sass']);
    gulp.watch('./tests/*/*.php', ['codecept']);
});

gulp.task('default', ['watch']);

